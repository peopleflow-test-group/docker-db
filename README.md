# docker-db

Docker-db project runs database for PeopleFlow group.

## Push / pull

```
docker build -t mishgun8ku/peopleflow-db:latest .  
docker push mishgun8ku/peopleflow-db:latest  
docker run -d --rm --name peopleflow-db -p 5432:5432 mishgun8ku/peopleflow-db:latest
```


## Database

jdbc:postgresql://localhost:5432/postgres

Host: localhost  
Port: 5432  
db: postgres  
  
user:       emp_man  
password:   emp_man
