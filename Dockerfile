FROM postgres:13-alpine
ENV POSTGRES_PASSWORD=postgres
RUN ["mkdir", "/script"]
COPY ./init.sh /docker-entrypoint-initdb.d/init.sh
COPY ./script /script
EXPOSE 5432